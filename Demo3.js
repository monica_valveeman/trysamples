//some
let Ar = [9, 8, 7, 6, 5];
let som = Ar.some(ar => ar % 2 === 0);
console.log(som);

let som1 = Ar.some(ar => ar % 2 != 0);
console.log(som1);

let som2 = Ar.some(ar => ar > 5);
console.log(som2);

let func = (x) => { x > 10 };
let som3 = Ar.some(func);
console.log(som3);

//every
let earr = [2, 3, 4, 5, 9];

let e1 = earr.every(x => x % 2 === 0);
console.log(e1);

let e2 = earr.every(x => x % 2 != 0);
console.log(e2);

let earr1 = [2, 4, 6, 8, 10];

let e4 = earr1.every(x => x % 2 === 0);
console.log(e4);

let earr2 = [1, 3, 5, 7, 9];

let e5 = earr2.every(x => x % 2 != 0);
console.log(e5);


let earr3 = [20, 80, 70, 4];

let e6 = earr3.every(x => x >= 10);
console.log(e6);

let pupop = ['yellow', 'green', 'blue', 'orange'];

//pop
let pop = pupop.pop();

console.log(pop);

console.log(pupop);

let pop1 = pupop.pop();

console.log(pop1);

console.log(pupop);

//push 
let push = pupop.push('orange');

console.log(push);

console.log(pupop);

let push1 = pupop.push('blue');

console.log(push1);

console.log(pupop);

//shift
let shift = pupop.shift();

console.log(shift);

console.log(pupop);

let shift1 = pupop.shift();

console.log(shift1);

console.log(pupop);

//unshift
let unshift = pupop.unshift('yellow');

console.log(unshift);

console.log(pupop);

let unshift1 = pupop.unshift('green');

console.log(unshift1);

console.log(pupop);


//entries,values,keys

let ent = ['violet', 'indigo', 'blue', 'green', 'yellow', 'orange', 'red'];
let entries = ent.entries();
for (let x of entries) {
    console.log(x);
    console.log(x.toString());
}

let values = ent.values();
for (let y of values) {
    console.log(y);
}

let keys = ent.keys();
for (let z of keys) {
    console.log(z);
}
